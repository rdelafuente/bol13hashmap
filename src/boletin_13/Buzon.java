/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin_13;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/** Version 1
 *
 * @author Silvano
 */
public class Buzon {
public static String contenidoCorreo(){
        return JOptionPane.showInputDialog("Contido do correo:");
    }
    /*
     * @param args the command line arguments
     */

    /**
     * @author Silvano
     * @param mail
     * @return 
     */
    public int numeroCorreos (ArrayList<Correo> mail){
        return mail.size();
    }
    /**
     * @author Silvano
     * @param mail 
     */
    public void añade (ArrayList<Correo> mail){
       mail.add(new Correo (contenidoCorreo()));
        salida("Correo añadido a la posición"+mail.size());
    }
    /**
     * @autor Silvano
     * @param email
     * @return 
     */
    public boolean porLeer(ArrayList<Correo> email){
        boolean aux = false;
        for(int i=0;i<email.size();i++){
            if(email.get(i).isLeido()==false){
                aux = true;
            }
        }
            return aux;
    }
    /**
     * @author Silvano
     * @param email
     * @return 
     */
    public String PrimerNoLeido(ArrayList<Correo> email){
        String aux = "";
        for(int i=0;i<email.size();i++){
            if(email.get(i).isLeido()==false) {
                aux = email.get(i).getContenido();
                email.get(i).setLeido(true);
                break;
            }else{
                aux = "Todos los mensajes están leidos";
            }
        }
        return aux;
        
    }
    /**
     * @author Silvano
     * @param k
     * @param email
     * @return 
     */ 
    public String muestra(int k, ArrayList<Correo> email){
        email.get(k).setLeido(true);
        return email.get(k).getContenido();
    }
    /**
     * metodo para eliminar correos
     * @author Ruben
     * @param k
     * @param email 
     * 
     */
    public void elimina(int k,ArrayList<Correo> email){
        salida("Correo eliminado \""+email.get(k).getContenido()+"\" en la posición "+(k+1));
        email.remove(k);
    }
    /**
     * @author Ruben
     * @return metodo para introducir el numero del correo
     */
    public static int introducirNumero(){
        String resposta = JOptionPane.showInputDialog("Introduce el número del correo:");
        return Integer.parseInt(resposta)-1;
    }
    /**
     * @author Ruben
     * @param saida 
     * @return Metodo para visualizar datos
     */
     public static void salida(String saida){
        JOptionPane.showMessageDialog(null, saida);
    }
    
    public static void main(String[] args) {
        ArrayList<Correo> mail = new ArrayList<>();
        Buzon obx = new Buzon();
        Correo correo = new Correo("....", false);
        String respuesta;
        int opcion;
        do{
            respuesta = JOptionPane.showInputDialog("Teclee la opción que quiera"
                    + "\n 1. Añadir un correo"
                    + "\n 2. Ver número de correos"
                    + "\n 3. Ver número de correos no leidos"
                    + "\n 4. Ver el primer correo no leído"
                    + "\n 5. Ver el número de correos introducidos por el usuario"
                    + "\n 6. Eliminar correos"
                    + "\n 0. Fin aplicación");
            opcion = Integer.parseInt(respuesta);
            switch (opcion){
                
                case 0:
                    break;
                case 1:obx.añade(mail);
                    break;
                case 2:obx.numeroCorreos(mail);
                    break;
                case 3:
                    if(obx.porLeer(mail)){
                        salida("Aun tienes mensajes sin leer");
                    }else {
                salida("No hay mensajes sin leer");
            }
                    break;
                case 4:
                    salida(obx.PrimerNoLeido(mail));
                    break;
                case 5:
                    salida(obx.muestra(introducirNumero(), mail));
                    break;
                case 6:
                    obx.elimina(introducirNumero(), mail);
                    break;
                    
                default:salida("Teclea unha opción correcta");
            }
        
        }while(opcion!=0);
    }

   
}
                    
        
   


