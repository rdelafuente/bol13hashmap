/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin_13;

/**
 * proba novamente de novo outra vez
 * @author Silvano
 */
public class Correo {
    
    private String contenido;
    private boolean leido;
    
    
   public Correo(){  
      
   }
   public Correo(String contenido,boolean leido){
     
       this.contenido = contenido;
       this.leido = leido;
   }
   
   public Correo (String contenido){
       
       this.contenido = contenido;
       leido = false;
    }   
/**
 * @author Ruben
 * @return 
 */
    public String getContenido() {
        return contenido;
    }
/**
 * @author Ruben
 * @param contenido 
 */
    public void setContenido(String contenido) {
        this.contenido = contenido;
   }

    /**
     * @author Silvano
     * @return the leido
     */
   public boolean isLeido() {
        return leido;
   }

    /**
     * @author Silvano
     * @param leido the leido to set
     */
   public void setLeido(boolean leido) {
        this.leido = leido;
   }
}

